package com.diofebrika.honeynet.DestPortStat;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.diofebrika.honeynet.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class DestPortStatActivity extends AppCompatActivity {

    int[] colorClassArray = new int[]{Color.parseColor("#e2c275"),Color.parseColor("#305f72"),
            Color.parseColor("#f17e7e")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dest_port_stat);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setTitle("Honeynet");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        BarChart destPortBarStat = (BarChart) findViewById(R.id.destport_barchart);

        BarDataSet barDataSet1 = new BarDataSet(barEntries(),"");
        barDataSet1.setColors(colorClassArray);
        barDataSet1.setValueTextSize(12);
        barDataSet1.setValueTextColor(Color.parseColor("#ffffff"));
        barDataSet1.setStackLabels(new String[]{"Dionaea","Cowrie","Glastopf"});

        BarData data = new BarData(barDataSet1);
        destPortBarStat.setData(data);
        destPortBarStat.setFitBars(true);
        destPortBarStat.setDrawValueAboveBar(false);
        destPortBarStat.setDragEnabled(true);
        destPortBarStat.setVisibleXRangeMaximum(6);
        destPortBarStat.getDescription().setEnabled(false);
        destPortBarStat.getLegend().setTextSize(13);

        String[] theDates = new String[] {"Jun 28","Jun 29","Jun 30","Jul 1","Jul 2","Jul 3"};
        XAxis xAxis = destPortBarStat.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(theDates));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(.5f);
        xAxis.setGranularityEnabled(true);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelRotationAngle(-55);

        float groupSpace = .1f;
        float barSpace = .3f;
        data.setBarWidth(.8f);

        destPortBarStat.getXAxis().setAxisMinimum(0);
        destPortBarStat.getXAxis().setAxisMaximum(0+destPortBarStat.getBarData().getGroupWidth(groupSpace,barSpace)*6);
        destPortBarStat.getAxisLeft().setAxisMinimum(0);
        destPortBarStat.invalidate();
        //BarChart

//        PieChart
        PieChart destPortPieStat = (PieChart) findViewById(R.id.destport_piechart);

        PieDataSet pieDataSet = new PieDataSet(pieEntries(),"");
        pieDataSet.setColors(colorClassArray);
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData = new PieData(pieDataSet);

//        typePieStat.setUsePercentValues(true);
        destPortPieStat .setCenterTextRadiusPercent(50);

        destPortPieStat .getDescription().setEnabled(false);
        destPortPieStat .getLegend().setTextSize(13);
        destPortPieStat .setData(pieData);
        destPortPieStat .invalidate();

        //PieChart

    }

    private ArrayList<BarEntry> barEntries(){

        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1, new float[]{15,20,30}));
        barEntries.add(new BarEntry(2, new float[]{20,22,10}));
        barEntries.add(new BarEntry(3,  new float[]{9,40,25}));
        barEntries.add(new BarEntry(4,  new float[]{15,11,18}));
        barEntries.add(new BarEntry(5,  new float[]{5,20,27}));
        barEntries.add(new BarEntry(6,  new float[]{4,33,21}));

        return barEntries;
    }

    private ArrayList<PieEntry> pieEntries(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }
}
