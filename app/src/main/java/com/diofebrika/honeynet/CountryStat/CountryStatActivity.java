package com.diofebrika.honeynet.CountryStat;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.diofebrika.honeynet.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class CountryStatActivity extends AppCompatActivity {

    int[] colorClassArray = new int[]{Color.parseColor("#e2c275"),Color.parseColor("#305f72"),
            Color.parseColor("#f17e7e")};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_stat);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setTitle("Honeynet");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        BarChart countryBarStat = (BarChart) findViewById(R.id.country_barchart);


        BarDataSet barDataSet1 = new BarDataSet(barEntries(),"");
        barDataSet1.setColors(colorClassArray);
        barDataSet1.setValueTextSize(12);
        barDataSet1.setValueTextColor(Color.parseColor("#ffffff"));
        barDataSet1.setStackLabels(new String[]{"Dionaea","Cowrie","Glastopf"});

        BarData data = new BarData(barDataSet1);
        countryBarStat.setData(data);
        countryBarStat.setFitBars(true);
        countryBarStat.setDrawValueAboveBar(false);
        countryBarStat.setDragEnabled(true);
        countryBarStat.setVisibleXRangeMaximum(6);
        countryBarStat.getDescription().setEnabled(false);
        countryBarStat.getLegend().setTextSize(13);

        String[] theDates = new String[] {"Jun 28","Jun 29","Jun 30","Jul 1","Jul 2","Jul 3"};
        XAxis xAxis = countryBarStat.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(theDates));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(.5f);
        xAxis.setGranularityEnabled(true);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelRotationAngle(-55);

        float groupSpace = .1f;
        float barSpace = .3f;
        data.setBarWidth(.8f);

        countryBarStat.getXAxis().setAxisMinimum(0);
        countryBarStat.getXAxis().setAxisMaximum(0+countryBarStat.getBarData().getGroupWidth(groupSpace,barSpace)*6);
        countryBarStat.getAxisLeft().setAxisMinimum(0);
        countryBarStat.invalidate();
        //BarChart

//        PieChart1
        PieChart countryPieStat1 = (PieChart) findViewById(R.id.country_piechart1);

        PieDataSet pieDataSet1 = new PieDataSet(pieEntries1(),"");
        pieDataSet1.setColors(colorClassArray);
        pieDataSet1.setValueTextSize(12);
        pieDataSet1.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData1 = new PieData(pieDataSet1);

        countryPieStat1.setCenterTextRadiusPercent(50);

        countryPieStat1.getDescription().setEnabled(false);
        countryPieStat1.getLegend().setTextSize(13);
        countryPieStat1.setData(pieData1);
        countryPieStat1.setCenterText("Russia");
        countryPieStat1.invalidate();

        //PieChart1

        //PieChart2
        PieChart countryPieStat2 = (PieChart) findViewById(R.id.country_piechart2);

        PieDataSet pieDataSet2 = new PieDataSet(pieEntries2(),"");
        pieDataSet2.setColors(colorClassArray);
        pieDataSet2.setValueTextSize(12);
        pieDataSet2.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData2 = new PieData(pieDataSet2);

        countryPieStat2.setCenterTextRadiusPercent(50);

        countryPieStat2.getDescription().setEnabled(false);
        countryPieStat2.getLegend().setTextSize(13);
        countryPieStat2.setData(pieData2);
        countryPieStat2.setCenterText("United States");
        countryPieStat2.invalidate();

        //PieChart2

        //PieChart3
        PieChart countryPieStat3 = (PieChart) findViewById(R.id.country_piechart3);

        PieDataSet pieDataSet3 = new PieDataSet(pieEntries3(),"");
        pieDataSet3.setColors(colorClassArray);
        pieDataSet3.setValueTextSize(12);
        pieDataSet3.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData3 = new PieData(pieDataSet3);

        countryPieStat3.setCenterTextRadiusPercent(50);

        countryPieStat3.getDescription().setEnabled(false);
        countryPieStat3.getLegend().setTextSize(13);
        countryPieStat3.setData(pieData3);
        countryPieStat3.setCenterText("Vietnam");
        countryPieStat3.invalidate();

        //PieChart3

        //PieChart4
        PieChart countryPieStat4 = (PieChart) findViewById(R.id.country_piechart4);

        PieDataSet pieDataSet4 = new PieDataSet(pieEntries4(),"");
        pieDataSet4.setColors(colorClassArray);
        pieDataSet4.setValueTextSize(12);
        pieDataSet4.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData4 = new PieData(pieDataSet4);

        countryPieStat4.setCenterTextRadiusPercent(50);

        countryPieStat4.getDescription().setEnabled(false);
        countryPieStat4.getLegend().setTextSize(13);
        countryPieStat4.setData(pieData4);
        countryPieStat4.setCenterText("Indonesia");
        countryPieStat4.invalidate();

        //PieChart4

        //PieChart5
        PieChart countryPieStat5 = (PieChart) findViewById(R.id.country_piechart5);

        PieDataSet pieDataSet5 = new PieDataSet(pieEntries5(),"");
        pieDataSet5.setColors(colorClassArray);
        pieDataSet5.setValueTextSize(12);
        pieDataSet5.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData5 = new PieData(pieDataSet5);

        countryPieStat5.setCenterTextRadiusPercent(50);

        countryPieStat5.getDescription().setEnabled(false);
        countryPieStat5.getLegend().setTextSize(13);
        countryPieStat5.setData(pieData5);
        countryPieStat5.setCenterText("India");
        countryPieStat5.invalidate();

        //PieChart5

        //PieChart6
        PieChart countryPieStat6 = (PieChart) findViewById(R.id.country_piechart6);

        PieDataSet pieDataSet6 = new PieDataSet(pieEntries6(),"");
        pieDataSet6.setColors(colorClassArray);
        pieDataSet6.setValueTextSize(12);
        pieDataSet6.setValueTextColor(Color.parseColor("#ffffff"));

        PieData pieData6 = new PieData(pieDataSet6);

        countryPieStat6.setCenterTextRadiusPercent(50);

        countryPieStat6.getDescription().setEnabled(false);
        countryPieStat6.getLegend().setTextSize(13);
        countryPieStat6.setData(pieData6);
        countryPieStat6.setCenterText("Hungary");
        countryPieStat6.invalidate();

        //PieChart6

    }

    private ArrayList<BarEntry> barEntries(){

        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1, new float[]{15,20,30}));
        barEntries.add(new BarEntry(2, new float[]{20,22,10}));
        barEntries.add(new BarEntry(3,  new float[]{9,40,25}));
        barEntries.add(new BarEntry(4,  new float[]{15,11,18}));
        barEntries.add(new BarEntry(5,  new float[]{5,20,27}));
        barEntries.add(new BarEntry(6,  new float[]{4,33,21}));

        return barEntries;
    }

    private ArrayList<PieEntry> pieEntries1(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }

    private ArrayList<PieEntry> pieEntries2(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }

    private ArrayList<PieEntry> pieEntries3(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }

    private ArrayList<PieEntry> pieEntries4(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }

    private ArrayList<PieEntry> pieEntries5(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }

    private ArrayList<PieEntry> pieEntries6(){
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(150,"Dionaea"));
        pieEntries.add(new PieEntry(100,"Cowrie"));
        pieEntries.add(new PieEntry(200,"Glastopf"));

        return pieEntries;
    }
}
