package com.diofebrika.honeynet.User;

import android.provider.ContactsContract;

public class User {

    private int id;
    private String username;
    private String email;
    private String password;
    private String picture;
    private String ipaddress;
    private int sensor;
    private int attackcount;
    private int uptime;


    public User() {
    }

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public User(int id, String username, String email, String password, String ipaddress, int attackcount) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.ipaddress = ipaddress;
        this.attackcount = attackcount;
    }

    public User(int id, String username, String email, String password, String picture, String ipaddress, int sensor, int attackcount, int uptime) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.picture = picture;
        this.ipaddress = ipaddress;
        this.sensor = sensor;
        this.attackcount = attackcount;
        this.uptime = uptime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    public int getSensor() {
        return sensor;
    }

    public void setSensor(int sensor) {
        this.sensor = sensor;
    }

    public int getAttackcount() {
        return attackcount;
    }

    public void setAttackcount(int attackcount) {
        this.attackcount = attackcount;
    }

    public int getUptime() {
        return uptime;
    }

    public void setUptime(int uptime) {
        this.uptime = uptime;
    }
}
