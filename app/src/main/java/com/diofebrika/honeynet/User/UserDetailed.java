package com.diofebrika.honeynet.User;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.model.AgentResponse;
import com.diofebrika.honeynet.network.ApiClient;
import com.diofebrika.honeynet.network.ApiService;

import org.w3c.dom.Text;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UserDetailed extends AppCompatActivity {


    AgentResponse agentResponses;
    ApiService apiService;
    CompositeDisposable disposable = new CompositeDisposable();
    String idAgent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detailed);

        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);

        TextView tvTitle = findViewById(R.id.tv_agentname);
        TextView tvIpAddress = findViewById(R.id.tv_ip_address);
        TextView tvUpTime = findViewById(R.id.tv_upTime);
        TextView tvStatus = findViewById(R.id.tv_status);
        Button btnRestart = findViewById(R.id.b_restart_agent);
        Button btnDestroy = findViewById(R.id.b_destroyButton);

        getIntents();
        idAgent = String.valueOf(agentResponses.getId());
        tvTitle.setText(agentResponses.getName());
        tvIpAddress.setText(agentResponses.getIpaddr());
        tvUpTime.setText(String.valueOf(agentResponses.getUptime()));
        tvStatus.setText(agentResponses.getStatus());

        btnRestart.setOnClickListener(v -> {
            updateUpTime();
        });
        btnDestroy.setOnClickListener(v ->{
            destroy();
        });


    }

    private void destroy() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses hapus ...");
        // show it
        progressDialog.show();
        disposable.add(
                apiService
                        .getDestroy(idAgent)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<AgentResponse>() {
                            @Override
                            public void onSuccess(AgentResponse agentResponses) {
                                Log.d("TAG", agentResponses.toString());
                                progressDialog.dismiss();
                                openBackDetail();
                            }
                            @Override
                            public void onError(Throwable e) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));
    }

    private void getIntents(){
        AgentResponse detail = (AgentResponse) getIntent().getSerializableExtra("detail");
        agentResponses = detail;
    }

    private void updateUpTime(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses update ...");
        // show it
        progressDialog.show();
        disposable.add(
                apiService
                        .updateUptime(idAgent, "1.0")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<AgentResponse>() {
                            @Override
                            public void onSuccess(AgentResponse agentResponses) {
                                Log.d("TAG", agentResponses.toString());
                                progressDialog.dismiss();
                                openBackDetail();
                            }
                            @Override
                            public void onError(Throwable e) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));
    }

    private void openBackDetail() {
        Intent intent = new Intent(getApplicationContext(), UserFragment.class);
        startActivity(intent);
        finish();
    }
}
