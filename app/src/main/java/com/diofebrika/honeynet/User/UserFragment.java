package com.diofebrika.honeynet.User;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.model.AgentResponse;
import com.diofebrika.honeynet.network.ApiClient;
import com.diofebrika.honeynet.network.ApiService;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class UserFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ListAdapter.Callback  {

    RecyclerView recyclerView;
    MaterialSearchView searchView;
    ApiService apiService;
    ListAdapter listAdapter;
    CompositeDisposable disposable = new CompositeDisposable();
    List<AgentResponse> data = new ArrayList<AgentResponse>();
    SwipeRefreshLayout swipeRefreshLayout;
    public static Fragment newInstance() {
        UserFragment fragment = new UserFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user,container,false);

        apiService = ApiClient.getClient(getActivity()).create(ApiService.class);


        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshUser);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Honeynet");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setHasOptionsMenu(true);

//        dummyDataset();
        getListAgent();

        listAdapter = new ListAdapter(data);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvUser);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);
        listAdapter.setCallback(this);

        searchView = (MaterialSearchView) view.findViewById(R.id.search_view);

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

                listAdapter = new ListAdapter(data);
                recyclerView.setAdapter(listAdapter);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText!=null && !newText.isEmpty()){
                    ArrayList<AgentResponse> lstFound = new ArrayList<AgentResponse>();
                    for (AgentResponse item:data){
                        String compare1 = item.getName();
                        if (compare1.toUpperCase().contains(newText.toUpperCase())){
                            lstFound.add(item);
                        }
                    }
                    listAdapter = new ListAdapter(lstFound);
                    recyclerView.setAdapter(listAdapter);
                }
                else {
                    listAdapter = new ListAdapter(data);
                    recyclerView.setAdapter(listAdapter);
                }
                return true;
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                if(swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(true);
                }
                // TODO Fetching data from server
                getListAgent();
            }
        });


        return view;
    }

    private void getListAgent(){
            disposable.add(
                    apiService
                            .getAgent()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(new Function<List<AgentResponse>, List<AgentResponse>>() {
                                @Override
                                public List<AgentResponse> apply(List<AgentResponse> beritas) throws Exception {
                                    Collections.sort(beritas, new Comparator<AgentResponse>() {
                                        @Override
                                        public int compare(AgentResponse n1, AgentResponse n2) {
                                            return n1.getId() - n2.getId();
                                        }
                                    });
                                    return beritas;
                                }
                            })
                            .subscribeWith(new DisposableSingleObserver<List<AgentResponse>>() {
                                @Override
                                public void onSuccess(List<AgentResponse> beritas) {
                                    data = beritas;
                                    listAdapter.addItems(beritas);
                                    listAdapter.notifyDataSetChanged();
                                    swipeRefreshLayout.setRefreshing(false);
                                }

                                @Override
                                public void onError(Throwable e) {
                                    swipeRefreshLayout.setRefreshing(false);
                                    Toast.makeText(getActivity(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                    Log.d("ERROR", "onError: " + e.getMessage());
                                }
                            }));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search,menu);
        MenuItem item= menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefresh() {
        getListAgent();
    }

    @Override
    public void onItemLocationListClick(int position) {
        Intent intent = new Intent(getActivity(), UserDetailed.class);
        intent.putExtra("detail", data.get(position));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }
}
