package com.diofebrika.honeynet.User;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.base.BaseViewHolder;
import com.diofebrika.honeynet.model.AgentResponse;


import java.util.List;

import butterknife.ButterKnife;

public class ListAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private ListAdapter.Callback mCallback;
    private List<AgentResponse> mBeritaList;
    private List<AgentResponse> mBeritaListDefault;
    private String mType;
//    Context context;

    public ListAdapter(List<AgentResponse> beritas) {
        mBeritaList = beritas;
    }

    public void setCallback(ListAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ListAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu_user, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new ListAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu_user, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mBeritaList != null && mBeritaList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mBeritaList != null && mBeritaList.size() > 0) {
            return mBeritaList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<AgentResponse> beritaList) {
        mBeritaList.clear();
        mBeritaList.addAll(beritaList);
        mBeritaListDefault = beritaList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvAgentName;


        public ViewHolder(View itemView) {
            super(itemView);
            tvAgentName = itemView.findViewById(R.id.tvUser);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            AgentResponse item = mBeritaList.get(position);
            Log.d("Debug",mBeritaList.toString());

            tvAgentName.setText(item.getName());

            itemView.setOnClickListener(v -> {
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }
}
