package com.diofebrika.honeynet.Statistics;

import java.sql.Date;

public class Statistics {

    private int id;
    private Date date;
    private int count;

    public Statistics() {
    }

    public Statistics(int id, Date date, int count) {
        this.id = id;
        this.date = date;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
