package com.diofebrika.honeynet.Statistics;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.diofebrika.honeynet.CountryStat.CountryStatActivity;
import com.diofebrika.honeynet.DestPortStat.DestPortStatActivity;
import com.diofebrika.honeynet.TypeStat.TypeStatActivity;

import com.diofebrika.honeynet.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;


public class StatisticFragment extends Fragment {

    LinearLayout bt_type,bt_country,bt_destport;


    public static StatisticFragment newInstance(){
        StatisticFragment fragment = new StatisticFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_statistic,container,false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Honeynet");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setHasOptionsMenu(true);

        LineChart lineChart = (LineChart) view.findViewById(R.id.stat_linechart);

        LineDataSet lineDataSet = new LineDataSet(lineChart(),"");
        lineDataSet.setColor(Color.parseColor("#e2c275"));
        lineDataSet.setLineWidth(3f);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillColor(Color.parseColor("#e0cea3"));

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        LineData data = new LineData(dataSets);
        lineChart.setData(data);
        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.invalidate();

        bt_type = (LinearLayout) view.findViewById(R.id.bt_type);
        bt_country = (LinearLayout) view.findViewById(R.id.bt_country);
        bt_destport = (LinearLayout) view.findViewById(R.id.bt_destport);

        bt_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TypeStatActivity.class);
                startActivity(intent);
            }
        });

        bt_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CountryStatActivity.class);
                startActivity(intent);
            }
        });

        bt_destport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DestPortStatActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    private ArrayList<Entry> lineChart(){
        ArrayList<Entry> lineChart = new ArrayList<Entry>();
        lineChart.add(new Entry(0,50));
        lineChart.add(new Entry(1,100));
        lineChart.add(new Entry(2,20));
        lineChart.add(new Entry(3,75));
        lineChart.add(new Entry(4,80));
        lineChart.add(new Entry(5,50));

        return lineChart;
    }

}
