package com.diofebrika.honeynet.model;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Geoip implements Serializable {

	@SerializedName("country")
	private String country;

	@SerializedName("country_code")
	private String countryCode;

	@SerializedName("city")
	private String city;

	@SerializedName("location")
	private Location location;

	@SerializedName("state")
	private String state;

	@SerializedName("postal_code")
	private Object postalCode;

	@SerializedName("autonomous_system_organization")
	private String autonomousSystemOrganization;

	@SerializedName("autonomous_system_number")
	private int autonomousSystemNumber;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setPostalCode(Object postalCode){
		this.postalCode = postalCode;
	}

	public Object getPostalCode(){
		return postalCode;
	}

	public void setAutonomousSystemOrganization(String autonomousSystemOrganization){
		this.autonomousSystemOrganization = autonomousSystemOrganization;
	}

	public String getAutonomousSystemOrganization(){
		return autonomousSystemOrganization;
	}

	public void setAutonomousSystemNumber(int autonomousSystemNumber){
		this.autonomousSystemNumber = autonomousSystemNumber;
	}

	public int getAutonomousSystemNumber(){
		return autonomousSystemNumber;
	}

	@Override
 	public String toString(){
		return 
			"Geoip{" + 
			"country = '" + country + '\'' + 
			",country_code = '" + countryCode + '\'' + 
			",city = '" + city + '\'' + 
			",location = '" + location + '\'' + 
			",state = '" + state + '\'' + 
			",postal_code = '" + postalCode + '\'' + 
			",autonomous_system_organization = '" + autonomousSystemOrganization + '\'' + 
			",autonomous_system_number = '" + autonomousSystemNumber + '\'' + 
			"}";
		}
}