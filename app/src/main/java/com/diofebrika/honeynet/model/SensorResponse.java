package com.diofebrika.honeynet.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SensorResponse implements Serializable {

	@SerializedName("registered_at")
	private String registeredAt;

	@SerializedName("agent_id")
	private int agentId;

	@SerializedName("type")
	private String type;

	@SerializedName("condition_id")
	private int conditionId;

	@SerializedName("uptime")
	private double uptime;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("ipaddr")
	private String ipaddr;

	@SerializedName("container_id")
	private String containerId;

	@SerializedName("string_id")
	private String stringId;

	@SerializedName("status")
	private String status;

	public void setRegisteredAt(String registeredAt){
		this.registeredAt = registeredAt;
	}

	public String getRegisteredAt(){
		return registeredAt;
	}

	public void setAgentId(int agentId){
		this.agentId = agentId;
	}

	public int getAgentId(){
		return agentId;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setConditionId(int conditionId){
		this.conditionId = conditionId;
	}

	public int getConditionId(){
		return conditionId;
	}

	public void setUptime(double uptime){
		this.uptime = uptime;
	}

	public double getUptime(){
		return uptime;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIpaddr(String ipaddr){
		this.ipaddr = ipaddr;
	}

	public String getIpaddr(){
		return ipaddr;
	}

	public void setContainerId(String containerId){
		this.containerId = containerId;
	}

	public String getContainerId(){
		return containerId;
	}

	public void setStringId(String stringId){
		this.stringId = stringId;
	}

	public String getStringId(){
		return stringId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SensorResponse{" + 
			"registered_at = '" + registeredAt + '\'' + 
			",agent_id = '" + agentId + '\'' + 
			",type = '" + type + '\'' + 
			",condition_id = '" + conditionId + '\'' + 
			",uptime = '" + uptime + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",ipaddr = '" + ipaddr + '\'' + 
			",container_id = '" + containerId + '\'' + 
			",string_id = '" + stringId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}