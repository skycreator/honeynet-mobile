package com.diofebrika.honeynet.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AgentResponse implements Serializable {

	@SerializedName("registered_at")
	private String registeredAt;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("ipaddr")
	private String ipaddr;

	@SerializedName("string_id")
	private String stringId;

	@SerializedName("container_id")
	private String containerId;

	@SerializedName("condition_id")
	private int conditionId;

	@SerializedName("uptime")
	private float uptime;

	@SerializedName("status")
	private String status;

	public void setRegisteredAt(String registeredAt){
		this.registeredAt = registeredAt;
	}

	public String getRegisteredAt(){
		return registeredAt;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIpaddr(String ipaddr){
		this.ipaddr = ipaddr;
	}

	public String getIpaddr(){
		return ipaddr;
	}

	public void setStringId(String stringId){
		this.stringId = stringId;
	}

	public String getStringId(){
		return stringId;
	}

	public void setContainerId(String containerId){
		this.containerId = containerId;
	}

	public String getContainerId(){
		return containerId;
	}

	public void setConditionId(int conditionId){
		this.conditionId = conditionId;
	}

	public int getConditionId(){
		return conditionId;
	}

	public void setUptime(float uptime){
		this.uptime = uptime;
	}

	public float getUptime(){
		return uptime;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AgentResponse{" + 
			"registered_at = '" + registeredAt + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",ipaddr = '" + ipaddr + '\'' + 
			",string_id = '" + stringId + '\'' + 
			",container_id = '" + containerId + '\'' + 
			",condition_id = '" + conditionId + '\'' + 
			",uptime = '" + uptime + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}