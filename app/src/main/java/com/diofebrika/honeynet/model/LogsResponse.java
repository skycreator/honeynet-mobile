package com.diofebrika.honeynet.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LogsResponse implements Serializable {

	@SerializedName("src_ip")
	private String srcIp;

	@SerializedName("src_port")
	private int srcPort;

	@SerializedName("agent_ip")
	private String agentIp;

	@SerializedName("identifier")
	private String identifier;

	@SerializedName("geoip")
	private Geoip geoip;

	@SerializedName("dst_port")
	private int dstPort;

	@SerializedName("connection")
	private Connection connection;

	@SerializedName("sensor")
	private String sensor;

	@SerializedName("_id")
	private String id;

	@SerializedName("src_hostname")
	private String srcHostname;

	@SerializedName("dst_ip")
	private String dstIp;

	@SerializedName("timestamp")
	private String timestamp;

	public void setSrcIp(String srcIp){
		this.srcIp = srcIp;
	}

	public String getSrcIp(){
		return srcIp;
	}

	public void setSrcPort(int srcPort){
		this.srcPort = srcPort;
	}

	public int getSrcPort(){
		return srcPort;
	}

	public void setAgentIp(String agentIp){
		this.agentIp = agentIp;
	}

	public String getAgentIp(){
		return agentIp;
	}

	public void setIdentifier(String identifier){
		this.identifier = identifier;
	}

	public String getIdentifier(){
		return identifier;
	}

	public void setGeoip(Geoip geoip){
		this.geoip = geoip;
	}

	public Geoip getGeoip(){
		return geoip;
	}

	public void setDstPort(int dstPort){
		this.dstPort = dstPort;
	}

	public int getDstPort(){
		return dstPort;
	}

	public void setConnection(Connection connection){
		this.connection = connection;
	}

	public Connection getConnection(){
		return connection;
	}

	public void setSensor(String sensor){
		this.sensor = sensor;
	}

	public String getSensor(){
		return sensor;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSrcHostname(String srcHostname){
		this.srcHostname = srcHostname;
	}

	public String getSrcHostname(){
		return srcHostname;
	}

	public void setDstIp(String dstIp){
		this.dstIp = dstIp;
	}

	public String getDstIp(){
		return dstIp;
	}

	public void setTimestamp(String timestamp){
		this.timestamp = timestamp;
	}

	public String getTimestamp(){
		return timestamp;
	}

	@Override
 	public String toString(){
		return 
			"LogsResponse{" + 
			"src_ip = '" + srcIp + '\'' + 
			",src_port = '" + srcPort + '\'' + 
			",agent_ip = '" + agentIp + '\'' + 
			",identifier = '" + identifier + '\'' + 
			",geoip = '" + geoip + '\'' + 
			",dst_port = '" + dstPort + '\'' + 
			",connection = '" + connection + '\'' + 
			",sensor = '" + sensor + '\'' + 
			",_id = '" + id + '\'' + 
			",src_hostname = '" + srcHostname + '\'' + 
			",dst_ip = '" + dstIp + '\'' + 
			",timestamp = '" + timestamp + '\'' + 
			"}";
		}
}