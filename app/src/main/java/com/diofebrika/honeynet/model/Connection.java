package com.diofebrika.honeynet.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Connection implements Serializable {

	@SerializedName("protocol")
	private String protocol;

	@SerializedName("transport")
	private String transport;

	@SerializedName("type")
	private String type;

	public void setProtocol(String protocol){
		this.protocol = protocol;
	}

	public String getProtocol(){
		return protocol;
	}

	public void setTransport(String transport){
		this.transport = transport;
	}

	public String getTransport(){
		return transport;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"Connection{" + 
			"protocol = '" + protocol + '\'' + 
			",transport = '" + transport + '\'' + 
			",type = '" + type + '\'' + 
			"}";
		}
}