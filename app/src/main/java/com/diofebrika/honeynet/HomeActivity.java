package com.diofebrika.honeynet;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.diofebrika.honeynet.DailyAttack.DailyAttackFragment;
import com.diofebrika.honeynet.Sensor.SensorFragment;
import com.diofebrika.honeynet.Statistics.StatisticFragment;
import com.diofebrika.honeynet.User.UserFragment;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    public static BottomNavigationView navigationMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (savedInstanceState == null){
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.frame, StatisticFragment.newInstance());
            transaction.commit();
        }

        navigationMenu = findViewById(R.id.navigation);
        navigationMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()){
                    case R.id.action_1:
                        selectedFragment = StatisticFragment.newInstance();
                        break;
                    case R.id.action_2:
                        selectedFragment = DailyAttackFragment.newInstance();
                        break;
                    case R.id.action_3:
                        selectedFragment = UserFragment.newInstance();
                        break;
                    case R.id.action_4:
                        selectedFragment = SensorFragment.newInstance();
                        break;
                }
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame,selectedFragment);
                transaction.commit();
                return true;
            }
        });
    }
}
