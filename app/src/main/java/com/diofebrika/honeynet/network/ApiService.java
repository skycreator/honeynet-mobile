package com.diofebrika.honeynet.network;

import com.diofebrika.honeynet.model.AgentResponse;
import com.diofebrika.honeynet.model.LogsResponse;
import com.diofebrika.honeynet.model.SensorResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @FormUrlEncoded
    @POST("api/agent/update/{id}")
    Single<AgentResponse> updateUptime(@Path("id") String id,
                                       @Field("uptime") String upTime);

    @GET("api/agent/delete/{id}")
    Single<AgentResponse> getDestroy(@Path("id") String id);

    @GET("api/agent")
    Single<List<AgentResponse>> getAgent();

    @POST("api/sensor/update/{id}")
    Single<SensorResponse> updateStatusSensor(@Path("id") String id,
                                        @Field("uptime") String upTime);

    @GET("api/sensor/delete/{id}")
    Single<SensorResponse> getDestroySensor(@Path("id") String id);

    @GET("api/sensor")
    Single<List<SensorResponse>> getSensor();

    @GET("logs/data/all")
    Single<List<LogsResponse>> getLogs();
//    @GET("user/detail/{id}")
//    Single<LoginResponse> getLurah(@Path("id") String id);
}
