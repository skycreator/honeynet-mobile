package com.diofebrika.honeynet.Sensor;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.model.SensorResponse;
import com.diofebrika.honeynet.network.ApiClient;
import com.diofebrika.honeynet.network.ApiService;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class SensorFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener, ListAdapterSensor.Callback{

    RecyclerView recyclerView;
    MaterialSearchView searchView;
    ApiService apiService;
    ListAdapterSensor listAdapter;
    CompositeDisposable disposable = new CompositeDisposable();
    List<SensorResponse> data = new ArrayList<SensorResponse>();
    SwipeRefreshLayout swipeRefreshLayout;
    public static Fragment newInstance() {
        SensorFragment fragment = new SensorFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user,container,false);

        apiService = ApiClient.getClient(getActivity()).create(ApiService.class);


        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshUser);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Honeynet");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setHasOptionsMenu(true);

//        dummyDataset();
        getListSensor();

        listAdapter = new ListAdapterSensor(data);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvUser);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);
        listAdapter.setCallback(this);

        searchView = (MaterialSearchView) view.findViewById(R.id.search_view);

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

                listAdapter = new ListAdapterSensor(data);
                recyclerView.setAdapter(listAdapter);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText!=null && !newText.isEmpty()){
                    ArrayList<SensorResponse> lstFound = new ArrayList<SensorResponse>();
                    for (SensorResponse item:data){
                        String compare1 = item.getName();
                        if (compare1.toUpperCase().contains(newText.toUpperCase())){
                            lstFound.add(item);
                        }
                    }
                    listAdapter = new ListAdapterSensor(lstFound);
                    recyclerView.setAdapter(listAdapter);
                }
                else {
                    listAdapter = new ListAdapterSensor(data);
                    recyclerView.setAdapter(listAdapter);
                }
                return true;
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                if(swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(true);
                }
                // TODO Fetching data from server
                getListSensor();
            }
        });


        return view;
    }

    private void getListSensor(){
        disposable.add(
                apiService
                        .getSensor()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<List<SensorResponse>, List<SensorResponse>>() {
                            @Override
                            public List<SensorResponse> apply(List<SensorResponse> beritas) throws Exception {
                                Collections.sort(beritas, new Comparator<SensorResponse>() {
                                    @Override
                                    public int compare(SensorResponse n1, SensorResponse n2) {
                                        return n1.getId() - n2.getId();
                                    }
                                });
                                return beritas;
                            }
                        })
                        .subscribeWith(new DisposableSingleObserver<List<SensorResponse>>() {
                            @Override
                            public void onSuccess(List<SensorResponse> beritas) {
                                data = beritas;
                                listAdapter.addItems(beritas);
                                listAdapter.notifyDataSetChanged();
                                swipeRefreshLayout.setRefreshing(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getActivity(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search,menu);
        MenuItem item= menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefresh() {
        getListSensor();
    }

    @Override
    public void onItemLocationListClick(int position) {
        Intent intent = new Intent(getActivity(), SensorDetailed.class);
        intent.putExtra("detail", data.get(position));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

}

