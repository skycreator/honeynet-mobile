package com.diofebrika.honeynet.Sensor;

public class Sensor {

    private int id;
    private String sensorName;
    private String type;
    private String userHandle;
    private String upTime;
    private int attackCount;

    public Sensor() {
    }


    public Sensor(int id, String sensorName) {
        this.id = id;
        this.sensorName = sensorName;
    }



    public Sensor(int id, String sensorName, String type, String userHandle, String upTime, int attackCount) {
        this.id = id;
        this.sensorName = sensorName;
        this.type = type;
        this.userHandle = userHandle;
        this.upTime = upTime;
        this.attackCount = attackCount;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserHandle() {
        return userHandle;
    }

    public void setUserHandle(String userHandle) {
        this.userHandle = userHandle;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public int getAttackCount() {
        return attackCount;
    }

    public void setAttackCount(int attackCount) {
        this.attackCount = attackCount;
    }
}
