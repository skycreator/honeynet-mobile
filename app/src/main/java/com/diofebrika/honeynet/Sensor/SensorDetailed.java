package com.diofebrika.honeynet.Sensor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.User.UserFragment;
import com.diofebrika.honeynet.model.AgentResponse;
import com.diofebrika.honeynet.model.SensorResponse;
import com.diofebrika.honeynet.network.ApiClient;
import com.diofebrika.honeynet.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SensorDetailed extends AppCompatActivity {


    SensorResponse sensorResponse;
    ApiService apiService;
    CompositeDisposable disposable = new CompositeDisposable();
    String idSensor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_detailed);

        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);

        TextView tvTitle = findViewById(R.id.tv_sensorName);
        TextView tvType = findViewById(R.id.tv_type);
        TextView tvIpAddr = findViewById(R.id.tv_ipAddr);
        TextView tvUpTime = findViewById(R.id.tv_upTime);
        TextView tvStatus = findViewById(R.id.tv_status);
        Button btnRestart = findViewById(R.id.b_restartSensor);
        Button btnStop = findViewById(R.id.b_stopButton);
        Button btnDestroy = findViewById(R.id.b_destroyButton);

        getIntents();
        idSensor = String.valueOf(sensorResponse.getId());
        tvTitle.setText(sensorResponse.getName());
        tvType.setText(sensorResponse.getType());
        tvIpAddr.setText(sensorResponse.getIpaddr());
        tvUpTime.setText(String.valueOf(sensorResponse.getUptime()));
        tvStatus.setText(sensorResponse.getStatus());

        btnRestart.setOnClickListener(v -> {
            updateUpTime();
        });
        btnStop.setOnClickListener(v -> {
            updateStatus();
        });
        btnDestroy.setOnClickListener(v ->{
            destroy();
        });


    }

    private void destroy() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses hapus ...");
        // show it
        progressDialog.show();
        disposable.add(
                apiService
                        .getDestroy(idSensor)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<AgentResponse>() {
                            @Override
                            public void onSuccess(AgentResponse agentResponses) {
                                Log.d("TAG", agentResponses.toString());
                                progressDialog.dismiss();
                                openBackDetail();
                            }
                            @Override
                            public void onError(Throwable e) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));
    }

    private void getIntents(){
        SensorResponse detail = (SensorResponse) getIntent().getSerializableExtra("detail");
        sensorResponse = detail;
    }

    private void updateUpTime(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses update ...");
        // show it
        progressDialog.show();
        disposable.add(
                apiService
                        .updateUptime(idSensor, "1.0")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<AgentResponse>() {
                            @Override
                            public void onSuccess(AgentResponse agentResponses) {
                                Log.d("TAG", agentResponses.toString());
                                progressDialog.dismiss();
                                openBackDetail();
                            }
                            @Override
                            public void onError(Throwable e) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));
    }

    private void updateStatus(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Proses update ...");
        // show it
        progressDialog.show();
        disposable.add(
                apiService
                        .updateStatusSensor(idSensor, "1.0")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<SensorResponse>() {
                            @Override
                            public void onSuccess(SensorResponse sensorResponse) {
                                Log.d("TAG", sensorResponse.toString());
                                progressDialog.dismiss();
                                openBackDetail();
                            }
                            @Override
                            public void onError(Throwable e) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));
    }
    private void openBackDetail() {
        Intent intent = new Intent(getApplicationContext(), UserFragment.class);
        startActivity(intent);
        finish();
    }
}
