package com.diofebrika.honeynet.Sensor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.base.BaseViewHolder;
import com.diofebrika.honeynet.model.AgentResponse;
import com.diofebrika.honeynet.model.SensorResponse;

import java.util.List;

import butterknife.ButterKnife;


public class ListAdapterSensor extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private ListAdapterSensor.Callback mCallback;
    private List<SensorResponse> mBeritaList;
    private List<SensorResponse> mBeritaListDefault;
    private String mType;
//    Context context;

    public ListAdapterSensor(List<SensorResponse> beritas) {
        mBeritaList = beritas;
    }

    public void setCallback(ListAdapterSensor.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ListAdapterSensor.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu_sensor, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new ListAdapterSensor.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu_sensor, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mBeritaList != null && mBeritaList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mBeritaList != null && mBeritaList.size() > 0) {
            return mBeritaList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<SensorResponse> beritaList) {
        mBeritaList.clear();
        mBeritaList.addAll(beritaList);
        mBeritaListDefault = beritaList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvSensorName;


        public ViewHolder(View itemView) {
            super(itemView);
            tvSensorName = itemView.findViewById(R.id.tvSensor);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            SensorResponse item = mBeritaList.get(position);
            Log.d("Debug",mBeritaList.toString());

            tvSensorName.setText(item.getName());

            itemView.setOnClickListener(v -> {
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }

}
