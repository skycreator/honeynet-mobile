package com.diofebrika.honeynet.DailyAttack;

import java.sql.Time;
import java.sql.Timestamp;

public class DailyAttack {

    private int id;
    private String ipaddressdaily;
    private String typedaily;
    private String regionaldaily;

    public DailyAttack() {

    }

    public DailyAttack(int id, String ipaddressdaily, String typedaily, String regionaldaily) {
        this.id = id;
        this.ipaddressdaily = ipaddressdaily;
        this.typedaily = typedaily;
        this.regionaldaily = regionaldaily;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getIpaddressdaily() {
        return ipaddressdaily;
    }

    public void setIpaddressdaily(String ipaddressdaily) {
        this.ipaddressdaily = ipaddressdaily;
    }

    public String getTypedaily() {
        return typedaily;
    }

    public void setTypedaily(String typedaily) {
        this.typedaily = typedaily;
    }

    public String getRegionaldaily() {
        return regionaldaily;
    }

    public void setRegionaldaily(String regionaldaily) {
        this.regionaldaily = regionaldaily;
    }
}
