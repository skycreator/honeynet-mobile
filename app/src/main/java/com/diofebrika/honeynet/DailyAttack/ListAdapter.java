package com.diofebrika.honeynet.DailyAttack;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.Sensor.ListAdapterSensor;
import com.diofebrika.honeynet.base.BaseViewHolder;
import com.diofebrika.honeynet.model.LogsResponse;
import com.diofebrika.honeynet.model.SensorResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class ListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private ListAdapter.Callback mCallback;
    private List<LogsResponse> mBeritaList;
    private List<LogsResponse> mBeritaListDefault;
    private String mType;
//    Context context;

    public ListAdapter(List<LogsResponse> beritas) {
        mBeritaList = beritas;
    }

    public void setCallback(ListAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ListAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu_daily, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new ListAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_menu_sensor, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mBeritaList != null && mBeritaList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mBeritaList != null && mBeritaList.size() > 0) {
            return mBeritaList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<LogsResponse> beritaList) {
        mBeritaList.clear();
        mBeritaList.addAll(beritaList);
        mBeritaListDefault = beritaList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView dailyAttackIp;
        TextView dailyAttackType;
        TextView dailyAttackRegional;


        public ViewHolder(View itemView) {
            super(itemView);

            dailyAttackIp= itemView.findViewById(R.id.tvipaddressdaily);
            dailyAttackType=itemView.findViewById(R.id.tvsensortypedaily);
            dailyAttackRegional= itemView.findViewById(R.id.tvregionaldaily);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            LogsResponse item = mBeritaList.get(position);
            Log.d("Debug",mBeritaList.toString());

            dailyAttackIp.setText(item.getSrcIp());
            dailyAttackType.setText(item.getSensor());
            dailyAttackRegional.setText(item.getGeoip().getCountry());

            itemView.setOnClickListener(v -> {
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }

}
