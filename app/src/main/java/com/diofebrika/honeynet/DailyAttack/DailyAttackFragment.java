package com.diofebrika.honeynet.DailyAttack;

import android.app.Fragment;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.diofebrika.honeynet.R;
import com.diofebrika.honeynet.Sensor.ListAdapterSensor;
import com.diofebrika.honeynet.model.LogsResponse;
import com.diofebrika.honeynet.model.SensorResponse;
import com.diofebrika.honeynet.network.ApiClient;
import com.diofebrika.honeynet.network.ApiClientSensor;
import com.diofebrika.honeynet.network.ApiService;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DailyAttackFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    MaterialSearchView searchView;
    RecyclerView recyclerView;
    ApiService apiService;
    ListAdapter listAdapter;
    CompositeDisposable disposable = new CompositeDisposable();
    List<LogsResponse> data = new ArrayList<LogsResponse>();
    SwipeRefreshLayout swipeRefreshLayout;

    public static Fragment newInstance() {
        DailyAttackFragment fragment = new DailyAttackFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_daily_attack,container,false);

        apiService = ApiClientSensor.getClient(getActivity()).create(ApiService.class);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshDaily);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Honeynet");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setHasOptionsMenu(true);

        getDailyAttack();

        listAdapter = new ListAdapter(data);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvDaily);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);

        searchView = (MaterialSearchView) view.findViewById(R.id.search_view);

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

                ListAdapter listAdapter = new ListAdapter(data);
                recyclerView.setAdapter(listAdapter);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText!=null && !newText.isEmpty()){
                    ArrayList<LogsResponse> lstFound = new ArrayList<LogsResponse>();
                    for (LogsResponse item:data){
                        String compare1 = item.getSrcIp();
                        String compare2 = item.getSensor();
                        String compare3 = item.getGeoip().getCountry();
                        if (compare1.contains(newText) || compare2.toUpperCase().contains(newText.toUpperCase()) ||
                                compare3.toUpperCase().contains(newText.toUpperCase())){
                            lstFound.add(item);
                        }
//                        if (compare1.contains(newText) || compare2.contains(newText) || compare3.contains(newText)){
//                            lstFound.add(item);
//                        }
                    }
//                    ListAdapterSensor adapter = new ListAdapterSensor(getActivity(),android.R.layout.simple_list_item_1,lstFound);
                    listAdapter = new ListAdapter(lstFound);
                    recyclerView.setAdapter(listAdapter);
                }
                else {
                    listAdapter = new ListAdapter(data);
                    recyclerView.setAdapter(listAdapter);
                }
                return true;
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                if(swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(true);
                }
                // TODO Fetching data from server
                getDailyAttack();
            }
        });

        return view;
    }

    private void getDailyAttack() {
        disposable.add(
                apiService
                        .getLogs()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<List<LogsResponse>, List<LogsResponse>>() {
                            @Override
                            public List<LogsResponse> apply(List<LogsResponse> beritas) throws Exception {
                                swipeRefreshLayout.setRefreshing(false);
                                return beritas;
                            }
                        })
                        .subscribeWith(new DisposableSingleObserver<List<LogsResponse>>() {
                            @Override
                            public void onSuccess(List<LogsResponse> beritas) {
                                data = beritas;
                                listAdapter.addItems(beritas);
                                listAdapter.notifyDataSetChanged();
                                swipeRefreshLayout.setRefreshing(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getActivity(), "Tidak terhubung server", Toast.LENGTH_SHORT).show();
                                Log.d("ERROR", "onError: " + e.getMessage());
                            }
                        }));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search,menu);
        MenuItem item= menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefresh() {

    }
}
